//
//  ViewController.swift
//  SampleExam
//
//  Created by Chris Chadillon on 2016-05-08.
//  Copyright © 2016 Chris Chadillon. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var theCountries = [Country]()
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCountries.count
    }
    
    @IBOutlet var theTableView: UITableView!
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell = self.theTableView.dequeueReusableCellWithIdentifier("theCell")
            cell!.textLabel?.text = theCountries[indexPath.row].theName
            return cell!
    }
    
    @IBOutlet var theSpinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loadCountries(sender: AnyObject) {
        self.theSpinner.startAnimating();
        GetCountries.getCountryListFromWebServices {
            (theCounrties:[Country]!)->() in
            dispatch_async(dispatch_get_main_queue(), {
                self.theSpinner.stopAnimating();
                self.theCountries = theCounrties
                self.theTableView.reloadData()
            })
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DetailsSegue" {
            let detailsVC = segue.destinationViewController as! DetailedViewComtrollerViewController
            detailsVC.aCountry = theCountries[(theTableView.indexPathForSelectedRow?.row)!]
        }
    }
    

}

