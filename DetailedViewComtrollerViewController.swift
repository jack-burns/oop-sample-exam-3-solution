//
//  DetailedViewComtrollerViewController.swift
//  SampleExam
//
//  Created by Chris Chadillon on 2016-05-08.
//  Copyright © 2016 Chris Chadillon. All rights reserved.
//

import UIKit

class DetailedViewComtrollerViewController: UIViewController {

    @IBOutlet var theName: UILabel!
    @IBOutlet var theTwoCode: UILabel!
    @IBOutlet var theThreeCode: UILabel!
    
    var aCountry:Country!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        theName.text = aCountry.theName
        theTwoCode.text = aCountry.theTwoCode
        theThreeCode.text = aCountry.theThreeCode
        
    }

    @IBAction func closeClicked(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion:nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
