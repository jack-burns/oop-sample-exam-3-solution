//
//  GetCountries.swift
//  SampleExam
//
//  Created by Chris Chadillon on 2016-05-08.
//  Copyright © 2016 Chris Chadillon. All rights reserved.
//

import Foundation

class GetCountries {
    
    class func getCountryListFromWebServices(completion:(theCounties:[Country]!)->()) {
        
        let theURL = "http://services.groupkt.com/country/get/all"
        
        let queue:dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        
        dispatch_async(queue, {
                        let theJSONResult: String!
                        do {
                            theJSONResult = try String(contentsOfURL: NSURL(string: theURL)!, encoding: NSUTF8StringEncoding)
                        }
                        catch _ {
                            theJSONResult = nil
                        }
                        
                        let jsonData:NSData! = theJSONResult.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
                        
                        let resultingDictionary = (try! NSJSONSerialization.JSONObjectWithData(jsonData, options: [])) as! NSDictionary
                        
                        let resultingDictionaryTwo = resultingDictionary.objectForKey("RestResponse") as! NSDictionary
                        
                        let resultingArray = resultingDictionaryTwo.objectForKey("result") as! NSArray
                        
                        var theCountries = [Country]()
                        
                        for aCountry in resultingArray {
                            let countryDictionary = aCountry as! NSDictionary
                            let aCountry = Country(theName: countryDictionary.objectForKey("name") as! String,
                                theTwoCode: countryDictionary.objectForKey("alpha2_code") as! String,
                                theThreeCode: countryDictionary.objectForKey("alpha3_code") as! String )
                            theCountries.append(aCountry)
                        }
                        
                        completion(theCounties: theCountries)
        })

    }
}
