//
//  Country.swift
//  SampleExam
//
//  Created by Chris Chadillon on 2016-05-08.
//  Copyright © 2016 Chris Chadillon. All rights reserved.
//

import Foundation

class Country {
    let theName:String
    let theTwoCode:String
    let theThreeCode:String
    
    init(theName:String, theTwoCode:String, theThreeCode:String){
        self.theName = theName
        self.theTwoCode = theTwoCode
        self.theThreeCode = theThreeCode
    }
}
